<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

require_once("AbstractModel.php");
require_once("Book.php");
require_once("dbCredencials.php");

/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;
    
    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
              $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PWD,
			array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            
        }
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookList($orderBy = 'id')
    {
		$orderBy = self::parseColumn($orderBy);
        $booklist = array();
		$stmt = $this->db->query(
		         'SELECT id, title, author, description '
				 ."FROM Book ORDER BY $orderBy"
		);
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$booklist[] = new Book($row['title'], $row['author'], 
			$row['description'], $row['id']);
		}
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookById($id)
    {
		self::verifyId($id);
		$stmt = $this->db->query(
				 "SELECT * FROM Book WHERE id = $id"
		);
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$book = new Book($row['title'], $row['author'], 
			$row['description'], $row['id']);
		}
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function addBook($book)
    {
		self::verifyBook($book);
		
		$stmt = $this->db->prepare(
				"INSERT INTO book (title, author, description)" 
				."VALUES(:title, :author, :description)"
				);
	
		$stmt->bindValue(':title', $book->title);
		$stmt->bindValue(':author', $book->author);
		$stmt->bindValue(':description', $book->description);
				
		$stmt->execute();
		$book->id = $this->db->lastInsertId();
	}

    /** Modifies data related to a book in the collection.
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function modifyBook($book)
    {
		self::verifyBook($book);
		
		$stmt = $this->db->prepare(
				"UPDATE book
				SET title=:title,
					author=:author,
					description=:description
					WHERE id=$book->id"
				);
	
		$stmt->bindValue(':title', $book->title);
		$stmt->bindValue(':author', $book->author);
		$stmt->bindValue(':description', $book->description);
				
		$stmt->execute();
		$book->id = $this->db->lastInsertId();
		
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {
		self::verifyId($id);
		$stmt = $this->db->prepare(
		"DELETE FROM book
		WHERE id=$id"
		);
		
		$stmt->execute();
		
    }
}

